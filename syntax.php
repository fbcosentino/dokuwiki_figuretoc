<?php
/**
 * DokuWiki Plugin figuretoc (Syntax Component)
 *
 * @license GPL 2 http://www.gnu.org/licenses/gpl-2.0.html
 * @author  Fernando Cosentino <fbcosentino@yahoo.com.br>
 * @url     https://bitbucket.org/fbcosentino/dokuwiki_figuretoc/
 */

// must be run within Dokuwiki
if (!defined('DOKU_INC')) {
    die();
}

if(!defined('DOKU_PLUGIN')) define('DOKU_PLUGIN',DOKU_INC.'lib/plugins/');
require_once(DOKU_PLUGIN.'syntax.php');

class syntax_plugin_figuretoc extends DokuWiki_Syntax_Plugin
{
	public $counter = 0;
	public $refs = array();
    /**
     * @return string Syntax mode type
     */
    public function getType()
    {
        return 'formatting';
    }

    /**
     * @return string Paragraph type
     */
    public function getPType()
    {
        return 'normal';
    }
	
    public function getAllowedTypes()
	{
        return array('formatting', 'substition', 'disabled');
    }	

    /**
     * @return int Sort order - Low numbers go before high numbers
     */
    public function getSort()
    {
        return 195; // Must be revised
    }

    /**
     * Connect lookup pattern to lexer.
     *
     * @param string $mode Parser mode
     */
    public function connectTo($mode)
    {
        $this->Lexer->addSpecialPattern('<@\[.*?\]>', $mode, 'plugin_figuretoc');
        $this->Lexer->addSpecialPattern('<\[.*?\]>', $mode, 'plugin_figuretoc');
        $this->Lexer->addSpecialPattern('<#\[.*?\]>', $mode, 'plugin_figuretoc');
    }
	/*
    public function postConnect()
    {
        $this->Lexer->addExitPattern('\]>', 'plugin_figuretoc');
    }*/

    /**
     * Handle matches of the figuretoc syntax
     *
     * @param string       $match   The match of the syntax
     * @param int          $state   The state of the handler
     * @param int          $pos     The position in the document
     * @param Doku_Handler $handler The handler
     *
     * @return array Data for the renderer
     */
    public function handle($match, $state, $pos, Doku_Handler $handler)
    {
		
		if (substr($match, 0, 3) == '<@[') {
			$data = array($state, $match, trim(substr($match, 3, -2)) );
		}
		elseif (substr($match, 0, 3) == '<#[') {
			$data = array($state, $match, (trim(substr($match, 3, -2))) );
		}
		else {
			$this->counter++;
			$fig_num = $this->counter;
			$desc = (trim(substr($match, 2, -2)));
			$this->refs[$desc] = $fig_num;
			
			$data = array($state, $match, $desc, $fig_num );
		}

        return $data;
    }

    /**
     * Render xhtml output or metadata
     *
     * @param string        $mode     Renderer mode (supported modes: xhtml)
     * @param Doku_Renderer $renderer The renderer
     * @param array         $data     The data from the handler() function
     *
     * @return bool If rendering was successful.
     */
    public function render($mode, Doku_Renderer $renderer, $data)
    {
		global $__FIGURE_LIST;
		
        if ($mode !== 'xhtml') {
            return false;
        }
		$state = $data[0];
		$match = $data[1];
		$desc  = $data[2];
		
		
		//if ($state == DOKU_LEXER_SPECIAL) {
		if (substr($match, 0, 3) == '<@[') {
			$htmldata = $renderer->_xmlEntities( $desc );
			$renderer->doc .= '<tocpagebreak name="Figures" toc-bookmarkText="' . $htmldata . '" toc-prehtml="&lt;h2&gt;' . $htmldata . '"&lt;/h2&gt; /><tocpagebreak/>';
		}

		elseif (substr($match, 0, 3) == '<#[') {
			if ($this->refs[$desc]) {
				$renderer->doc .= $this->refs[$desc];
			}
			else {
				$renderer->doc .= '?';
			}
		}
		
		else {
			$text = "Fig. ".($data[3])." - ".$desc;
			$renderer->doc .= '<tocentry name="Figures" content="' . $renderer->_xmlEntities($text) . '" />'
								.'<center><div align="center" class="figures_caption">' . $text . '</div></center>'; 
			
		}
		

        return true;
    }
}

