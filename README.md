# figuretoc #

This plugin simplifies adding a List of Figures in a PDF exported via dw2pdf in a dokuwiki.

## How to Use ##

### How to add the List ###

Where you want the list of figures, insert:

<@[Title of the List of Figures Page]>

Example:

<@[List of Figures]>

Recommended places are at the very begining or very end. All text between this line and the next top-level header will be grouped in the PDF's TOC.

### How to add captions to the list ###

Right above or under a figure (depending on your layout preference), insert:

<[Text for the caption]>

Do not use the words "Figure", "Fig." or manually type numbers. Numbering will be automatic. In The example above, the caption will become (both in the list and in the text):

Fig. 1 - Text for the caption

